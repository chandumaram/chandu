//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContactDetail
    {
        public string UserName { get; set; }
        public long PhNo { get; set; }
        public Nullable<long> LandLine { get; set; }
        public string HAddress { get; set; }
        public string OAddress { get; set; }
    
        public virtual PersonalDetail PersonalDetail { get; set; }
    }
}
